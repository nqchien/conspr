#!/usr/bin/env python
# Module solver.py

# Class Flow and Domain
# Flow solver for 2DH domain on a uniform rectangular grid
# using finite volume method

# Written by Nguyen Quang Chien, 2009
# Numerical method based on XBeach, http://xbeach.org/
# Revised 2021, 2022, updated with the version on GitHub
# River case: flow along x-axis

# License GNU GPL, http://www.gnu.org/copyleft/gpl.html
 
import numpy as np
from copy import deepcopy


class Param(object):
    """ Parameters related to the flow """
    def __init__(self):
        # hydraulic parameters
        self.Ch = 65        # Chezy coefficient, m^0.5 s^-1 
        self.rho = 1000     # water density, kg m^-3
        self.eps = 0.1      # diffusivity, m^2 s-1
        self.g = 9.81       # acceleration of gravity, m^2 s^-1
        self.hmin = 0.001   # minimum water depth, m
        self.umin = 0.1     # minimum flow velocity, m s^-1
        self.CFL = 0.4      # Courant-Fredricht-Lewy number
        self.zs0 = 0        # initial water level, m
        self.v0 = 1.0       # initial velocity, m s^-1
        self.i = 1.E-4      # longitudinal slope
        
        # timing parameters
        self.tstart = 0     # start time of simulation, s
        self.t = self.tstart
        self.tint = 1       # time interval, s
        self.tnext = 0 + self.tint
        self.tstop = 100    # end time of simulation, s
        
        # contaminant parameters
        self.k = 0          # decay rate, s^-1
        self.Elong = 1      # dispersion rate in longitudinal direction, m^2 s^-1
        self.Etran = 0.5      # dispersion rate in transversal direction, m^2 s^-1


class Domain(object):
    """ Computational domain """
    def __init__(self, flowpar, type='river-ideal', ext=None):
        self.type = type
        self.makeBathy(flowpar, type, 4, 0, 0.5)
        
        # Forcing etc.
        domainDim = np.shape(self.zb)
        self.Fx = np.zeros(domainDim)       # wave force, x-component
        self.Fy = np.zeros(domainDim)       # wave force, y-component
        self.W = np.zeros(domainDim)        # pollutant loading
        
        # Initial flow condition
        zs0 = flowpar.zs0                   # initial surface level
        zs = np.zeros(domainDim) - flowpar.i * self.x
        self.zs = np.maximum(zs, self.zb)   # surface level
        self.h = self.zs - self.zb          # water depth
        self.dzsdt = np.zeros(domainDim)    # rate of change, water level
        self.dzbdt = np.zeros(domainDim)    # rate of change, bed level
        self.uu = np.zeros(domainDim)       # u-velocity at u point
        self.vv = np.zeros(domainDim)       # v-velocity at v point
        self.uu = flowpar.Ch * np.sqrt(self.h * flowpar.i)  # Chezy formula
        self.uu = self.uu * (self.h > 0)    # masking out dry points
        self.qx = np.zeros(domainDim)       # flux in x-direction
        self.qy = np.zeros(domainDim)       # flux in y-direction
        self.c = np.zeros(domainDim)        # concentration


    def makeBathy(self, flowpar, kind, *arg):
        """ Creates the bathymetry for the model.
            Parameters
            ----------
            `flowpar`: instance of `Param`
                parameters of the flow
            `kind`: `str`
                the kind of bathymetry
                valid values include `'river-ideal'` and
                `'San Francisco bay'`
            
            `*arg`: `tuple`
                providing additional info in case `kind == 'river-ideal'`
                * `h0` the maximum river depth
                * `slope_t` the transversal slope
                * `bank_height` the river bank height
        """
        if kind.strip().lower() == 'river-ideal':
            if not 'ext' in locals():
                ext = (100, 50, 40, 10)
            (self.nx, self.ny, self.dx, self.dy) = ext
            domainDim = (self.nx + 1, self.ny + 1)
            self.x = np.zeros(domainDim)
            self.y = np.zeros(domainDim)
            self.zb = np.zeros(domainDim)
            h0 = arg[0]             # max. river depth
            slope_t = arg[1]        # transversal slope
            bank_height = arg[2]    # river bank height
            for j in range(self.ny + 1):
                for i in range(self.nx + 1):
                    self.x[i,j] = i * self.dx
                    self.y[i,j] = j * self.dy
                    self.zb[i,j] = -h0 + self.y[i,j] * slope_t
            self.zb[:, :5] = bank_height     # river bank on northern and southern sides
            self.zb[:, -5:] = bank_height     # river bank on northern and southern sides
            self.zb -= flowpar.i * self.x     # Longitudinal slope i (x-direction)
        elif kind.strip().lower() == 'san francisco bay':
            # using GEBCO bathymetry data
            self.nx = 251
            self.ny = 298
            CELLSIZE = 0.004166666667
            # Length of 1 degree lon/lat: http://www.csgnetwork.com/degreelenllavcalc.html
            LEN1DEGLON = 88189.03
            LEN1DEGLAT = 110990.78
            self.dx = int(CELLSIZE * LEN1DEGLON)
            self.dy = int(CELLSIZE * LEN1DEGLAT)
            XLL = -122.816666666667
            YLL = 37.237500000000
            
            self.x, self.y = np.meshgrid(
                    np.arange(XLL, XLL + self.nx*self.dx + 0.1, self.dx), 
                    np.arange(YLL, YLL + self.ny*self.dy + 0.1, self.dy))
            self.zb = np.loadtxt('SF/bathy.asc', delimiter=' ', skiprows=6).reshape(self.x.shape)
            print('Domain size:', self.x.shape, self.y.shape, self.zb.shape)


    def solve(self, flowpar):
        g = flowpar.g           # gravitational acceleration
        Ch = flowpar.Ch         # Chezy bed roughness coefficient
        rho = flowpar.rho       # water density
        umin = flowpar.umin     # minimum velocity
        hmin = flowpar.hmin     # minimum water depth
        eps = flowpar.eps
        t = flowpar.t           # simulation time
        tint = flowpar.tint     # time interval
        tnext = flowpar.tnext   # next time layer
        CFL = flowpar.CFL       # minimum velocity
        Elong = flowpar.Elong   # alongstream dispersion coefficient
        Etran = flowpar.Etran   # transversal dispersion coefficient

        x = deepcopy(self.x)
        y = deepcopy(self.y)
        dx = deepcopy(self.dx)
        dy = deepcopy(self.dy)
        uu  = deepcopy(self.uu)
        vv = deepcopy(self.vv)
        qx = deepcopy(self.qx)
        qy = deepcopy(self.qy)
        zb = deepcopy(self.zb)
        zs = deepcopy(self.zs)
        Fx = deepcopy(self.Fx)
        Fy = deepcopy(self.Fy)
        c = deepcopy(self.c)
        W = deepcopy(self.W)

        domainDim = np.shape(x)
        vu      = np.zeros(domainDim)       # v-velocity at u point
        vsu     = np.zeros(domainDim)       # Stoke v-velocity at u point
        usu     = np.zeros(domainDim)       # Stoke u-velocity at u point
        vsv     = np.zeros(domainDim)       # Stoke v-velocity at v point
        usv     = np.zeros(domainDim)       # Stoke u-velocity at v point
        uv      = np.zeros(domainDim)       # u-velocity at v point
        vmagu   = np.zeros(domainDim)       # v-velocity magnitude at u point
        vmageu  = np.zeros(domainDim)       # Euler v-velocity magnitude at u point
        vmagv   = np.zeros(domainDim)       # v-velocity magnitude at v point
        vmagev  = np.zeros(domainDim)       # Euler v-velocity magnitude at v point
        veu     = np.zeros(domainDim)       # Euler v-velocity at u point
        ueu     = np.zeros(domainDim)       # Euler u-velocity at u point
        vev     = np.zeros(domainDim)       # Euler v-velocity at v point
        uev     = np.zeros(domainDim)       # Euler u-velocity at v point
        u       = np.zeros(domainDim)       # u-velocity at cell centre (for plotting)
        v       = np.zeros(domainDim)       # v-velocity at cell centre (for plotting)
        dzsdx   = np.zeros(domainDim)       # water level gradient, x-direction
        dzsdy   = np.zeros(domainDim)       # water level gradient, y-direction
        dzsdt   = np.zeros(domainDim)       # rate of change, water level
        dcdt    = np.zeros(domainDim)       # rate of change, concentration
        mx      = np.zeros(domainDim)       # coefficient matrix for x-momentum-based water depth
        mx1     = np.zeros(domainDim)       # coefficient matrix for x-momentum-based water depth
        mx2     = np.zeros(domainDim)       # coefficient matrix for x-momentum-based water depth
        my      = np.zeros(domainDim)       # coefficient matrix for y-momentum-based water depth
        my1     = np.zeros(domainDim)       # coefficient matrix for y-momentum-based water depth
        my2     = np.zeros(domainDim)       # coefficient matrix for y-momentum-based water depth
        f1      = np.zeros(domainDim)       # momentum-based water depth/concenctration at cell's west/south side
        f2      = np.zeros(domainDim)       # momentum-based water depth/concenctration at cell's east/north side
        ududx   = np.zeros(domainDim)       # advection term (determined using momentum conserving method)
        vdudy   = np.zeros(domainDim)       # advection term (determined using momentum conserving method)
        udvdx   = np.zeros(domainDim)       # advection term (determined using momentum conserving method)
        vdvdy   = np.zeros(domainDim)       # advection term (determined using momentum conserving method)
        # us      = np.zeros(domainDim)       # Stoke u-velocity at cell centre (for plotting)
        # vs      = np.zeros(domainDim)       # Stoke v-velocity at cell centre (for plotting)
        ue      = np.zeros(domainDim)       # Euler u-velocity at cell centre (for plotting)
        ve      = np.zeros(domainDim)       # Euler v-velocity at cell centre (for plotting)
        Ecx     = np.zeros(domainDim)       # diffusive flux, x-direction
        Ecy     = np.zeros(domainDim)       # diffusive flux, y-direction

        # Velocities at u-points
        vu[:-1, 1:-1] = 0.25 * (vv[:-1, :-2] + vv[:-1, 1:-1] +
			                    vv[1:, :-2] + vv[1:, 1:-1])
        # ... at boundaries
        vu[:,0] = vu[:,1]
        vu[:, -1] = vu[:, -2]
        
        # ... Stokes
        vsu[:-2, 1:-2] = 0
        vsu[:, 0] = vsu[:, 1]
        vsu[:, -1] = vsu[:, -2]
        usu[:-2, 1:-2] = 0
        usu[:, 0] = usu[:, 1]
        usu[:, -1] = usu[:, -2]
        
        # ... Euler
        veu = vu - vsu
        ueu = uu - usu

        # ... magnitude
        vmagu = np.sqrt(uu**2 + vu**2)
        vmageu = np.sqrt(ueu**2 + veu**2)

        # Velocities at v-points
        uv[1:-1, :-1] = 0.25 * (uu[:-2, :-1] + uu[1:-1, :-1] +
                				uu[:-2, 1:] + uu[1:-1, 1:])
        # ... at boundary
        uv[:, -1] = uv[:, -2]
        uv[:, 0] = uv[:, 1]

        # ... Stokes
        vsv[1:-1, :-1] = 0
        vsv[:, 0] = vsv[:, 1]
        vsv[:, -1] = vsv[:, -2]
        usv[1:-1, :-1] = 0
        usv[:, 0] = usv[:, 1]
        usv[:, -1] = usv[:, -2]

        # ... Euler
        vev = vv - vsv
        uev = uv - usv
        vmagv = np.sqrt(uv**2 + vv**2)
        vmagev = np.sqrt(uev**2 + vev**2)

        # Water surface slopes
        dzsdx[:-1, :] = (zs[1:, :] - zs[:-1, :]) / dx
        dzsdy[:, :-1] = (zs[:, 1:] - zs[:, :-1]) / dy

        # Upwind method implemented through weight factors mx1 and mx2
        
        h = np.maximum(zs - zb, hmin)    # water depth
        # weth = h > hmin

        # X-direction
        mx = np.sign(uu)
        mx[np.abs(uu) < umin] = 0 
        mx1 =  (mx + 1) / 2
        mx2 = -(mx - 1) / 2
        f1[:-1, :] = h[:-1, :]
        f2[:-1, :] = h[1:, :]
        
        hu = mx1 * f1 + mx2 * f2            # h at u-points for cont. eq.: upwind
        hum = np.maximum(0.5 * (f1 + f2), hmin) # h at u-points for mom. eq.: mean

        f1.fill(0)
        f2.fill(0)
        f1[1:-1, :] = (qx[1:-1, :] + qx[:-2,:]) * (uu[1:-1, :] - uu[:-2,:]) / (2 * dx)
        f2[:-2, :] = f1[1:-1,:]
        ududx = 1 / hum * (mx1 * f1 + mx2 * f2)
        vdudy[:, 1:-1] = vu[:, 1:-1] * (uu[:, 2:] - uu[:, :-2]) / (2 * dy)
        
        wetu = hu > eps     # wetting & drying crit. (only for mom. balance)
        ur = uu[1,:]        # store velocity at upstream boundary (given by BC)

        # Timestep based on the dominant velocity component, u
        dt = CFL * np.min(dx / (np.sqrt(g*h) 
                            + np.maximum(vmagu,vmageu)))
        dt = np.maximum(dt, 0.05)
        t += dt
        if t >= tnext:
            dt -= t - tnext
            t = tnext
            tnext += tint
        
        # Explicit Euler step momentum u-direction
        uu[wetu] = uu[wetu] - dt * ( ududx[wetu] + vdudy[wetu]
                                     + g * dzsdx[wetu]
                                     + g / (Ch**2) / hu[wetu] * vmageu[wetu] * ueu[wetu]  # GLM approach
                                     - Fx[wetu] / rho / hu[wetu] )

        uu[wetu==False] = 0

        # uu[-1, :] = 0
        # uu[-2, :] = 0   # reflection at the last grid line (usually dry)
        uu[1,:] = ur    # restore the seaward boundary condition

        qx = uu * hu    # flux at u-point
        
        # Y-direction
        my = np.sign(vv)
        my[np.abs(vv) < umin] = 0
        my1 =  (my + 1) / 2
        my2 = -(my - 1) / 2
        f1.fill(0)
        f2.fill(0)
        f1[:, :-1] = h[:, :-1]
        f2[:, :-1] = h[:, 1:]
        
        hv = my1 * f1 + my2 * f2    # h at v-points for cont. eq.: upwind
        hvm = 0.5 * (f1 + f2)       # h at v-points for mom. eq.: mean
        
        f1.fill(0)
        f2.fill(0)
        f1[:, 1:-1] = 0.5 * (qy[:, 1:-1] + qy[:, :-2]) * (vv[:, 1:-1] - vv[:, :-2]) / dy
        f2[:, :-2] = f1[:, 1:-1]
        vdvdy = 1 / hvm * (my1 * f1 + my2 * f2)
        udvdx[1:-1, :] = uv[1:-1,:] * (vv[2:, :] - vv[:-2, :]) / (2 * dx)
        
        wetv = hv > eps  # wetting & drying crit. (only for mom. balance)
        
        # Explicit Euler step momentum v-direction
        vv[wetv] = vv[wetv] - dt * ( udvdx[wetv] + vdvdy[wetv] 
                                     + g * dzsdy[wetv] 
                                     + g / Ch**2 / hv[wetv] * vmagev[wetv] * vev[wetv] 
                                     - Fy[wetv] / rho / hv[wetv] )
        vv[wetv==False] = 0
        
        qy = vv * hv    # flux at v-points
        
        #  Velocities at cell centre
        u[1:-1, :] = 0.5 * (uu[:-2, :] + uu[1:-1, :])
        u[0, :] = uu[0, :]
        v[:, 1:-1] = 0.5 * (vv[:, :-2] + vv[:, 1:-1])
        v[:, 0] = vv[:, 0]
        
        ue[1:-1, :] = 0.5 * (ueu[:-2, :] + ueu[1:-1,:])
        ue[0, :] = ueu[0, :]
        ve[:, 1:-1] = 0.5 * (vev[:, :-2] + vev[:, 1:-1])
        ve[:, 0] = vev[:, 0]
        
        # Update water level using continuity eq.
        dzsdt[1:-1, 1:-1] = - (qx[1:-1, 1:-1] - qx[:-2, 1:-1]) / dx \
                            - (qy[1:-1, 1:-1] - qy[1:-1, :-2]) / dy
        
        zs[1:-1, 1:-1] += dzsdt[1:-1, 1:-1] * dt
        

        # Transport of contaminants
        k = flowpar.k

        # at boundaries
        c[0, :] = c[1, :]
        c[-1, :] = c[-2, :]
        c[:, 0] = c[:, 1]
        c[:, -1] = c[:, -2]
        
        # X-direction
        f1.fill(0)
        f2.fill(0)
        f1[:-1, :] = c[:-1, :]
        f2[:-1, :] = c[1:, :]
        cu = mx1 * f1 + mx2 * f2
        Su = cu * hu * uu
        Ecx[1:-1, :] = Elong * ( 
                0.5 * (h[:-2, :] + h[1:-1, :]) * (c[:-2, :] - c[1:-1, :]) * wetu[:-2, :] * wetu[1:-1, :]
                + 0.5 * (h[1:-1, :] + h[2:, :])  * (c[2:, :] - c[1:-1, :]) * wetu[2:, :] * wetu[1:-1, :]
                ) / (dx*dx) / h[1:-1, :]

        # Y-direction
        f1.fill(0)
        f2.fill(0)
        f1[:, :-1] = c[:, :-1]
        f2[:, :-1] = c[:, 1:]
        cv = my * f1 + my2 * f2
        Sv = cv * hv * vv
        Ecy[:, 1:-1] = Etran * (  0.5  * wetv[:, :-2] * wetv[:, 1:-1] * 
									(h[:, :-2] + h[:, 1:-1]) *	(c[:, :-2] - c[:, 1:-1])
								+ 0.5  * wetv[:, 2:] * wetv[:, 1:-1] *
									(h[:, 1:-1] + h[:, 2:]) * (c[:, 2:] - c[:, 1:-1])
								) / (dy*dy) / h[:, 1:-1]
        
        # Update using mass balance
        dcdt[1:-1, 1:-1] =  \
                - ((Su[1:-1, 1:-1] - Su[:-2, 1:-1]) / dx + \
                   (Sv[1:-1, 1:-1] - Sv[1:-1, :-2]) / dy ) / h[1:-1, 1:-1] \
                + Ecx[1:-1, 1:-1] \
                + Ecy[1:-1, 1:-1] \
                + W[1:-1, 1:-1] / (h[1:-1, 1:-1] * dx * dy) \
                - k * c[1:-1, 1:-1]

        c[1:-1, 1:-1] += dcdt[1:-1, 1:-1] * dt
        c[wetv==False] = 0
        c[wetu==False] = 0
        
        # Output
        self.uu = uu
        self.vv  = vv 
        self.ueu = ueu
        self.vev = vev 
        self.qx = qx
        self.qy = qy 
        self.vmagu = vmagu
        self.vmagv = vmagv
        self.u = u
        self.v = v 
        self.ue = ue
        self.ve = ve 
        self.zs = zs
        self.hold = h
        self.h = np.maximum(zs - zb, hmin)
        self.wetu = wetu
        self.wetv = wetv
        self.hu = hu
        self.hv = hv
        self.Su = Su
        self.Sv = Sv
        self.c = c

        flowpar.dt = dt
        flowpar.t = t
        flowpar.tnext = tnext
