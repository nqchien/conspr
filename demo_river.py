# Demonstrate using `conspr` via the example
# of river flow and pollutant spreading
# Tutorial: https://bitbucket.org/nqchien/conspr/src/master/tutorial.md

import numpy
from solver import Param, Domain
from pylab import pcolor, quiver, colorbar, plot, figure, title, axis, gca, grid, legend, show

par = Param()
river = Domain(par, type='river-ideal')

# pcolor(river.x, river.y, river.zb); colorbar()
# quiver(river.x, river.y, river.uu, river.vv); axis('equal')

# figure(2)   # Plot the river bank
# plot(river.x[:,20], river.zb[:,20], 'k', river.x[:,20], river.zs[:,20], 'b')
# show()

for i in range(100):
    river.solve(par)

assert par.t > 0, 'Error simulation!'
assert par.dt > 0, 'Error simulation!'

# Pollutant spill
xmin_spill = river.nx//5 - 1
xmax_spill = river.nx//5 + 1
ymin_spill = river.ny//2 - 2
ymax_spill = river.ny//2 + 2
river.c[xmin_spill:xmax_spill, ymin_spill:ymax_spill] = 2

xmid_spill = (xmin_spill + xmax_spill)//2
xrange_spill = xmax_spill - xmin_spill
ymid_spill = (ymin_spill + ymax_spill)//2
yrange_spill = ymax_spill - ymin_spill

for i in range(200):
    river.solve(par)

figure(figsize=(8,4))
pcolor(river.x, river.y, river.c, shading='auto', cmap='bone_r'); 
colorbar(location='bottom'); 
quiver(river.x, river.y, river.uu, river.vv)
title('Concentration of pollutant in the river')
ax = gca()
# ax.set_xlim([])
ax.set_ylim([0, 500])
axis('equal'); show()

figure(figsize=(6,3))
plot(river.y[xmid_spill,:], numpy.transpose(river.c[xmin_spill-xrange_spill:xmax_spill+xrange_spill,:]))
title('Concentration on river cross-sections')
legend([f'XS {i}' for i in range(xmin_spill-xrange_spill,xmax_spill+xrange_spill)])
grid(True)

figure(figsize=(6,3))
plot(river.x[:,ymid_spill], river.c[:, ymin_spill-yrange_spill:ymax_spill+yrange_spill])
title('Concentration along river profiles')
legend([f'Profile {i}' for i in range(ymin_spill-yrange_spill, ymax_spill+yrange_spill)])
grid(True)
show()