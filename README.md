## Introduction 

You might want to experiment with the phenomena of contaminant spreading in a natural water bodies]. While there are several methods of approaches (field observation, lab work), one of the most popular method is to use a numerical model. `Conspr` is such a model.


## Structure 
In `conspr`, the natural water body is divided into non-overlapping control volumes. The solver employs a finite volume method to find the flow field. 


## Implementation 

`conspr` is written in Python. This programming language is quite straightforward to understand and appealing for newcomers. It can be embedded into web-notebooks e.g. Jupyter so that users can get a visual impression of the system. Of course, real-world applications require much more complex models / softwares, however conspr can be used in preliminary analyses as well as for teaching.